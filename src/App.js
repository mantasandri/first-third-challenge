import React, {useState, useEffect} from "react";
import List from "./componenets/List";
import SignUp from './componenets/SignUp';
import './styles.css';

const API_URL = "mock-api.json";

function App() {
  const [list, setList] = useState(null);
  const [error, setError] = useState(null);
  const [selectedList, setSelectedList] = useState([]);

  const getAPI = () => {
    fetch(`${API_URL}`)
      .then(res => {
        if (res.status >= 400) {
          setError('Server responds with errror!');
        }
        return res.json();
      })
      .then(response => {
        setList(response);
      })
      .catch(error => {
        setError(error);
      });
  };

  const updateSelection = (item) => {
    selectedList.includes(item) ? handleRemove(item) : handleAdd(item);
  };

  const handleAdd = (item) => {
    setSelectedList([...selectedList, item]);
  };

  const handleRemove = (item) => {
    setSelectedList(selectedList.filter((t) => t !== item));
  };

  useEffect(() => {
    getAPI();
  }, []);

  return (
    <div className="app">
      <header className="header">
        <h1 className="header-text">Newsletters</h1>
        <p>Select all the newsletters you’d like to receive</p>
      </header>
      {error ? error : ""}
      <List list={list} selectedList={updateSelection} />
      <SignUp selectedList={selectedList} />
    </div>
  );
}

export default App;
