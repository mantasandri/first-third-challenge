import React, { useState } from "react";
import ListItem from "./ListItem";

const List = ({list, selectedList}) => {
  const [checked, setChecked] = useState([]);

  const handleChecked = (item) => {
    checked.includes(item) ? handleRemove(item) : handleAdd(item);
    selectedList(item);
  };

  const handleAdd = (item) => {
    setChecked([...checked, item]);
  };

  const handleRemove = (item) => {
    setChecked(checked.filter((t) => t !== item));
  };

  return (
    <>
      {list && list.length
        ? list.map((item) => {
            return (
              <ListItem
                key={item._id}
                item={item}
                onClickedItem={handleChecked}
              />
            );
          })
        : "Not found"}
    </>
  );
};

export default List;
