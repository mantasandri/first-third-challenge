import React, {useState} from "react";

const ListItem = ({item, onClickedItem}) => {
  const [checked, setChecked] = useState(false);

  const handleClick = (item) => {
    setChecked(!checked);
    onClickedItem(item.signup);
  };

  return (
    <div className="listitem">
      <div className="listitem-image">
        <img src={item.picture} alt={item.company} />
      </div>
      <div className="listitem-info">
        <h1 className="listitem-info__title">{item.title}</h1>
        <p className="listitem-info__description">{item.description}</p>
      </div>
      <div className="listitem-checkbox">
        <div
          className={
            checked
              ? "listitem-checkbox__container listitem-checkbox__container--checked"
              : "listitem-checkbox__container"
          }
        >
          <input
            type="checkbox"
            className="listitem-checkbox__container--checkbox"
            value={item.signup}
            onChange={() => handleClick(item)}
          />
          <svg
            className="listitem-checkbox__container--svg"
            viewBox="0 0 20 20"
          >
            <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
          </svg>
        </div>
        <label></label>
      </div>
    </div>
  );
};

export default ListItem;
