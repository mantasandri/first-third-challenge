import React, {useState} from "react";

const SignUp = ({selectedList}) => {
  const [checked, setChecked] = useState(false);
  const [showPopup, setShowPopup] = useState(false);

  const emailRef = React.useRef();

  const handleCheckbox = (value) => {
    setChecked(value);
  };

  const handleSubmission = () => {
    // unsubbed?
    // console.log(`Unsub? ${checked}`);

    // mock submission
    setTimeout(() => {
      if (validateEmail && selectedList.length) setShowPopup(true);
    }, 2000);
  };

  const validateEmail = () => {
    const email = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return email.test(String(emailRef.current.value).toLowerCase());
  };

  const random = Math.random() >= 0.5;

  const modal = (
    <div className="modal">
      <div className="modal-content">
        <span className="modal-close" onClick={() => setShowPopup(false)}>
          &times;
        </span>
        {random ? "Successfully signed up! :)" : "Failed to sign up! :("}
      </div>
    </div>
  );

  return (
    <>
      {showPopup ? modal : ""}
      <form className="signup" onSubmit={handleSubmission}>
        <div className="signup-container">
          <input
            className="signup-container__input"
            type="text"
            placeholder="Enter email"
            name="email"
            required
            ref={emailRef}
          />
          <button
            className="signup-container__button"
            type="button"
            onClick={handleSubmission}
          >
            SUBSCRIBE
          </button>
        </div>
        <div className="unsub-container">
          <input
            className="unsub-container__input"
            type="checkbox"
            onChange={(e) => handleCheckbox(!checked)}
            checked={checked}
          />
          <label
            onClick={(e) => handleCheckbox(!checked)}
            className="unsub-container__label"
          >
            I do not want to recieve information about future newsletters
          </label>
        </div>
      </form>
    </>
  );
};

export default SignUp;
